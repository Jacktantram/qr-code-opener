$(function () {


    function makeCode(value){
        var qrcode = new QRCode("qrcode");
        qrcode.makeCode(value);

    }
    chrome.tabs.query({currentWindow: true, active: true}, function(tabs){
        makeCode(tabs[0].url);
    });

});